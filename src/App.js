import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import Header from "./Components/Header/Header";
import LifeCyclePage from "./Pages/LifeCyclePage/LifeCyclePage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import HookPage from "./Pages/HookPage/HookPage";
import TaiXiuPage from "./Pages/TaiXiuPage/TaiXiuPage";
import UseEffectPage from "./Pages/UseEffectPage/UseEffectPage";
import UsersPage from "./Pages/UsersPage/UsersPage";

function App() {
  return (
    <div className="text-center">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/home" element={<HomePage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/life-cycle" element={<LifeCyclePage />} />
          <Route path="/detail/:id" element={<DetailPage />} />
          <Route path="/learn-hook" element={<HookPage />} />
          <Route path="/tai-xiu" element={<TaiXiuPage />} />
          <Route path="/use-effect-page" element={<UseEffectPage />} />
          <Route path="/users" element={<UsersPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
