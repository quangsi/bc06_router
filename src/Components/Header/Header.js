import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class Header extends Component {
  routeArr = [
    {
      path: "/home",
      title: "Home",
    },
    {
      path: "/login",
      title: "Login",
    },
    {
      path: "/life-cycle",
      title: "Life Cycle",
    },
    {
      path: "/learn-hook",
      title: " Learn Hook",
    },
    {
      path: "/tai-xiu",
      title: " Game",
    },
    {
      path: "/use-effect-page",
      title: "UseEffect",
    },
    {
      path: "/users",
      title: "User List",
    },
  ];
  render() {
    return (
      <div className="p-5">
        {this.routeArr.map((route, index) => {
          return (
            <NavLink
              key={index}
              className="px-3 py-1 rounded bg-warning mx-3"
              to={route.path}
            >
              {route.title}
            </NavLink>
          );
        })}
      </div>
    );
  }
}
