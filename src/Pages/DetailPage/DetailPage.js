import axios from "axios";
import React, { Component } from "react";
import { withRouter } from "../../HOC/witRouter";
import { Card } from "antd";
const { Meta } = Card;
class DetailPage extends Component {
  state = {
    movie: {},
  };
  componentDidMount() {
    let { id } = this.props.router.params;
    axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MSIsIkhldEhhblN0cmluZyI6IjEyLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDQ3NjgwMDAwMCIsIm5iZiI6MTY2NTI0ODQwMCwiZXhwIjoxNjk0NjI0NDAwfQ.SUELcPShU58ZkNS3CbFDhM02KMzll9j00ndjVSaiJ8Q",
      },
    })
      .then((res) => {
        console.log(res);
        this.setState({ movie: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    console.log(this.props);
    return (
      <div>
        <Card
          hoverable
          style={{
            width: 240,
          }}
          cover={<img alt="example" src={this.state.movie.hinhAnh} />}
        >
          <Meta
            title={this.state.movie.tenPhim}
            description="www.instagram.com"
          />
        </Card>
      </div>
    );
  }
}

export default withRouter(DetailPage);

// card antd

// antd  npm
