import axios from "axios";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Header from "../../Components/Header/Header";

export default class HomePage extends Component {
  state = {
    movieList: [],
  };
  componentDidMount() {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05",
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MSIsIkhldEhhblN0cmluZyI6IjEyLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDQ3NjgwMDAwMCIsIm5iZiI6MTY2NTI0ODQwMCwiZXhwIjoxNjk0NjI0NDAwfQ.SUELcPShU58ZkNS3CbFDhM02KMzll9j00ndjVSaiJ8Q",
      },
    })
      .then((res) => {
        this.setState({ movieList: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  renderMovieList = () => {
    return this.state.movieList.map((item) => {
      return (
        <div className="card text-left col-3">
          <img
            style={{ height: 160, objectFit: "cover", objectPosition: "top" }}
            className="card-img-top "
            src={item.hinhAnh}
            alt
          />
          <div className="card-body">
            <h4 className="card-title">{item.tenPhim}</h4>
            <p className="card-text">
              {item.moTa.length > 50
                ? item.moTa.slice(0, 50) + "... Đọc thêm"
                : item.moTa}
            </p>
            <NavLink to={`/detail/${item.maPhim}`} className="btn btn-danger">
              Xem chi tiết
            </NavLink>
          </div>
        </div>
      );
    });
  };
  //  row col-4 card bs4
  render() {
    return (
      <div className="container pt-5">
        <h1 className="text-primary">HomePage</h1>
        <div className="row">{this.renderMovieList()}</div>
      </div>
    );
  }
}
