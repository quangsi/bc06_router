import React from "react";

export default function Checkout(props) {
  console.log("props", props);
  return (
    <div className="p-5 bg-warning">
      <h2>Checkout</h2>

      <strong>Total money : {props.number * 100} $</strong>

      <br />
      <button onClick={props.handleThanhToan} className="btn btn-secondary">
        Pay now
      </button>
    </div>
  );
}
