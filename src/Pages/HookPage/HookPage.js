import React, { useState } from "react";
import Checkout from "./Checkout";
// KHÔNG DÙNG THIS
export default function HookPage() {
  let [number, setNumber] = useState(1);
  console.log("number", number);
  let handleTang = () => {
    setNumber(number + 1);
  };
  let handleGiam = () => {
    setNumber(number - 1);
  };
  let handleThanhToan = () => {
    setNumber(0);
  };
  return (
    <div>
      <button onClick={handleGiam} className="btn btn-danger">
        -
      </button>
      <strong className="px-5">{number}</strong>
      <button onClick={handleTang} className="btn btn-success">
        +
      </button>
      <Checkout handleThanhToan={handleThanhToan} number={number} />
    </div>
  );
}

// rfc
