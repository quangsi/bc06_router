import React, { Component } from "react";
import Nav from "./Nav";

export default class LifeCyclePage extends Component {
  state = { number: 1 };
  componentDidMount() {
    // dùng để gọi api khi user load trang
    console.log("Parent - Didmount");
    // setInterval(() => {
    //   this.handleDecrease();
    // }, 1);
  }
  handleIncrease = () => this.setState({ number: this.state.number + 1 });
  handleDecrease = () => this.setState({ number: this.state.number - 1 });
  shouldComponentUpdate(nextProps, nextState) {
    // return true => render lại, return false => ko render
    if (nextState.number == 3) {
      return false;
    } else {
      return true;
    }
  }
  render() {
    console.log("Parent - Rerender");
    return (
      <div>
        <h2>LifeCyclePage</h2>
        {this.state.number < 5 && <Nav />}
        <button onClick={this.handleDecrease} className="btn btn-danger">
          -
        </button>
        <strong className="mx-3">{this.state.number}</strong>
        <button onClick={this.handleIncrease} className="btn btn-success">
          +
        </button>
      </div>
    );
  }
  componentDidUpdate(preProps, preState) {
    // tự động được gọi sau khi render chạy xong
    console.log("old", preState);
    console.log("current", this.state);
    console.log("DidUpdate");
  }
}
