import React, { Component, PureComponent } from "react";

export default class Nav extends PureComponent {
  componentDidMount() {
    console.log("Child - Didmount");
    let time = 100;
    // this.myCoundonw = setInterval(() => {
    //   console.log("🚀 - file: Nav.js:7 - time", time);
    //   time--;
    // }, 10);
  }

  render() {
    console.log("Child render");
    return <div className="p-5 bg-primary">Nav</div>;
  }
  componentWillUnmount() {
    // clearInterval(this.myCoundonw);
    console.log("Child - WillUnmount");
  }
}
