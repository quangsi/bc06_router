import React, { Component } from "react";

export default class LoginPage extends Component {
  state = {
    username: "Alice",
    password: "",
  };
  hanleOnChangeLogin = (e) => {
    console.log(e.target.name);
    let { value, name } = e.target;
    this.setState({ [name]: value });
  };
  render() {
    return (
      <div className="container pt-5">
        <h1 className="text-success">LoginPage</h1>

        <form>
          <div className="form-group">
            <input
              onChange={this.hanleOnChangeLogin}
              value={this.state.username}
              type="text"
              className="form-control"
              name="username"
              placeholder="username"
            />
          </div>
          <div className="form-group">
            <input
              onChange={this.hanleOnChangeLogin}
              type="text"
              className="form-control"
              name="password"
              placeholder="password"
            />
          </div>
          <button type="button" className="btn btn-warning">
            Login
          </button>
        </form>
      </div>
    );
  }
}

let user = {
  name: "alice",
  age: 2,
};

user.name = "tomy";
user["name"] = "tomy";

let key = "age";
let value = 20;
key = "name";
value = "tomy";
// user["name"] = "tomy";

user[key] = value;

// 50 51 55
