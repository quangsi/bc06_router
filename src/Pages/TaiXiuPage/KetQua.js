import React from "react";
import { TAI, XIU } from "./TaiXiuPage";

export default function KetQua({
  luaChon,
  handlePlayGame,
  soBanThang,
  soLuotChoi,
}) {
  return (
    <div>
      <button onClick={handlePlayGame} className="btn btn-warning px-5 py-3">
        Play Game
      </button>
      <h1 className="my-5 "></h1>
      {luaChon && <h2 className="my-5">Bạn chọn : {luaChon}</h2>}
      <h2 className="my-5">Số bàn thắng : {soBanThang}</h2>
      <h2>Số lượt chơi : {soLuotChoi}</h2>
    </div>
  );
}
