import React, { useState } from "react";
import bg_game from "../../assets/bgGame.png";
import "./game.css";
import KetQua from "./KetQua";
import XucXac from "./XucXac";

export const TAI = "TÀI";
export const XIU = "XỈU";
export default function TaiXiuPage() {
  const [xucXacArr, setXucXacArr] = useState([
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
  ]);
  const [luaChon, setLuaChon] = useState(null);
  const [soBanThang, setSoBangThang] = useState(0);
  const [soLuotChoi, setSoLuotChoi] = useState(0);

  const handleDatCuoc = (value) => {
    setLuaChon(value);
  };

  const handlePlayGame = () => {
    // Math.floor(Math.random() * (max - min + 1) + min)
    console.log("yes");
    let tongDiem = 0;
    let newXucXacArr = xucXacArr.map(() => {
      let number = Math.floor(Math.random() * 6) + 1;
      tongDiem += number;
      return {
        img: `./imgXucXac/${number}.png`,
        giaTri: number,
      };
    });
    setXucXacArr(newXucXacArr);
    setSoLuotChoi(soLuotChoi + 1);
    tongDiem >= 11 && luaChon == TAI && setSoBangThang(soBanThang + 1);
    tongDiem < 11 && luaChon == XIU && setSoBangThang(soBanThang + 1);
  };

  return (
    <div
      className="game_container"
      style={{ backgroundImage: `url(${bg_game})` }}
    >
      <XucXac handleDatCuoc={handleDatCuoc} xucXacArr={xucXacArr} />
      <KetQua
        soBanThang={soBanThang}
        soLuotChoi={soLuotChoi}
        handlePlayGame={handlePlayGame}
        luaChon={luaChon}
      />
    </div>
  );
}
// Math.random

// tổng >=11 : Tài
