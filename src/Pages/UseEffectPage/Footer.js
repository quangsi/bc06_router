import React, { memo } from "react";

function Footer({ like }) {
  console.log("footer render");
  return <div className="py-5 bg-warning ">Footer - Like : {like}</div>;
}
export default memo(Footer);

// memo ~ PureComponent
