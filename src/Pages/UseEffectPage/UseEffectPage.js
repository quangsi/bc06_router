import React, { useCallback, useEffect, useMemo, useState } from "react";
import Footer from "./Footer";

export default function UseEffectPage() {
  const [like, setLike] = useState(0);
  const [share, setShare] = useState(0);
  useEffect(() => {
    // console.log("did mount");
    // console.log("did update - like");
  }, [like]);
  //   useEffect : function 2 tham số:   ()=>{}  và dependency list []
  //   useEffect ~ didMount, didUpdate (dependency list)
  //   nếu giá trị trong dependency list thay đổi thì sẽ gọi lại tham số thứ nhất chạy
  useEffect(() => {
    // console.log("did mount");
    // console.log("did update - share");
  }, [share]);
  const handlePlusLike = useCallback(() => {
    setLike(like + 1);
  }, [like]);
  // useCallback : ghi nhớ lại function => xử lý function

  // useMemo => xử lý kết quả tính toán
  let doubleLike = useMemo(() => {
    return like * 2;
  }, [like]);
  const handlePlusShare = () => {
    setShare(share + 1);
  };
  console.log("render");
  return (
    <div>
      <Footer like={like} handlePlusLike={handlePlusLike} />
      <strong className="text-success mx-5 display-4">{like}</strong>
      <button onClick={handlePlusLike} className="btn btn-success">
        Plus like
      </button>
      <br />
      <strong className="text-danger mx-5 display-4">{share}</strong>
      <button onClick={handlePlusShare} className="btn btn-danger">
        Plus share
      </button>
    </div>
  );
}
