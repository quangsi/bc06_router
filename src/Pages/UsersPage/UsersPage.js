import { Table } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";

let headers = [
  {
    title: "Tài khoản ",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Họ tên ",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Email ",
    dataIndex: "email",
    key: "email",
  },
];

export default function UsersPage() {
  const [userList, setUserList] = useState([]);
  useEffect(() => {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP00",
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MSIsIkhldEhhblN0cmluZyI6IjEyLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDQ3NjgwMDAwMCIsIm5iZiI6MTY2NTI0ODQwMCwiZXhwIjoxNjk0NjI0NDAwfQ.SUELcPShU58ZkNS3CbFDhM02KMzll9j00ndjVSaiJ8Q",
      },
    })
      .then((res) => {
        setUserList(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div>
      <Table
        rowKey={(user) => {
          return user.taiKhoan;
        }}
        columns={headers}
        dataSource={userList}
      />
    </div>
  );
}
